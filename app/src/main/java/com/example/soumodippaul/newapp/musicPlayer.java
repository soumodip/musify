package com.example.soumodippaul.newapp;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.LoginManager;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class musicPlayer extends AppCompatActivity {

    ImageView usrImage;
    ImageView openPlayList;
    ImageView userLogout;
    SharedPreferences pref;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pref = getApplicationContext().getSharedPreferences("com.example.soumodippaul.newapp", MODE_PRIVATE);
        editor = pref.edit();
        setContentView(R.layout.activity_music_player);
        String[] songNameArray=new String[]{
                "Baatein_Kuch_Ankahee_Si",
                "Bhula_Dena",
                "Bol_Do_Na_Zara",
                "Enna_Sona",
                "Gori_Teri_Ankhen",
                "Love_You_Zindagi",
                "Morey_Saiyan",
                "Piya_Aaye_Na",
                "Pyaar_Manga_Hai",
                "Tujhe_Bhula_Diya"
        };
        String[] singerNameArray=new String[]{
                "Adnan Sami and Pritam",
                "Arijit Singh",
                "Armaan Malik",
                "Arijit Singh",
                "Lucky Ali",
                "Jasleen Royal",
                "Unknown",
                "KK and Jeet Ganguli",
                "Armaan Malik",
                "Mohit Chouhan"
        };
        CustomListAdapter adapter=new CustomListAdapter(this,songNameArray,singerNameArray);
        //ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,R.layout.song_list_view,R.id.songName,value);
        ListView songsList= (ListView) findViewById(R.id.songsList);
        songsList.setAdapter(adapter);
        songsList.setDivider(null);
        setFontandText();
    }

    public void setFontandText(){
        //CHANGE FONT
        Typeface josephSans=Typeface.createFromAsset(getAssets(),"fonts/josephSans.ttf");
        TextView usrName=(TextView)findViewById(R.id.userName);
        TextView usrEmail=(TextView)findViewById(R.id.userEmail);
        usrImage = (ImageView) findViewById(R.id.userImage);
        openPlayList = (ImageView) findViewById(R.id.openPlaylist);
        userLogout = (ImageView) findViewById(R.id.userLogout);
        usrName.setTypeface(josephSans);
        usrEmail.setTypeface(josephSans);
        //SET TEXT
        String name=getIntent().getStringExtra("NAME");
        String email=getIntent().getStringExtra("EMAIL");
        usrName.setText(name);
        usrEmail.setText(email);
        //CALL AYNC METHOD AND LOAD IMAGE
        loadImageAsync loadImg=new loadImageAsync();
        loadImg.execute();
        //OPEN PLAYLIST
        openPlayList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),playlistActivity.class);
                startActivity(intent);
            }
        });
        userLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginManager.getInstance().logOut();
                finish();
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });
    }

    //LOADS IMAGE IN ASYNC MODE
    public class loadImageAsync extends AsyncTask<String, Void, Bitmap>{
        @Override
        protected Bitmap doInBackground(String... params) {
            // TODO Auto-generated method stub
            try {
                URL url = new URL(getIntent().getStringExtra("PROFILE_PIC"));
                InputStream is = url.openConnection().getInputStream();
                Bitmap bitMap = BitmapFactory.decodeStream(is);
                return bitMap;
            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            usrImage.setImageBitmap(getRoundedCornerBitmap(bitmap,250));
        }
    }

    //MAKES IMAGE ROUNDED
    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
                .getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = pixels;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

    public void getSharedPreferences(){
        for (int i=0;i<10;i++){
            String tempStr=(i+1)+"";
            Log.d("SHARED",pref.getBoolean(tempStr,false)+"");
        }
    }

    @Override
    public void onBackPressed() {
        finishAffinity();
    }
}
