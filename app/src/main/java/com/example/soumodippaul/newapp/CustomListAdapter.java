package com.example.soumodippaul.newapp;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import static android.content.Context.MODE_PRIVATE;
import static com.facebook.FacebookSdk.getApplicationContext;

public class CustomListAdapter extends ArrayAdapter<String> {

      private final Activity context;
      private final String[] songName;
      private final String[] singerName;
      private final Typeface josephSans;
      private final int[][] colorArray=new int[][]{
              {255,204,0},
              {23,166,135},
              {43,135,200},
              {142,76,160}
      };
    SharedPreferences pref;
    SharedPreferences.Editor editor;

      public CustomListAdapter(Activity context,String[] songName,String[] singerName){
          super(context,R.layout.song_list_view,songName);
          this.context=context;
          this.songName=songName;
          this.singerName=singerName;
          josephSans=Typeface.createFromAsset(context.getAssets(),"fonts/josephSans.ttf");
          pref = context.getSharedPreferences("com.example.soumodippaul.newapp", MODE_PRIVATE);
          editor = pref.edit();
      }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflator=context.getLayoutInflater();
        View rowView=inflator.inflate(R.layout.song_list_view,null,true);
        TextView songNameTxt=(TextView) rowView.findViewById(R.id.individualSongName);
        TextView singerNameTxt=(TextView) rowView.findViewById(R.id.individualSongSinger);
        ImageView openMusicPlayerBtn= (ImageView) rowView.findViewById(R.id.openMusicPlayer);
        ImageView addToPlaylist= (ImageView) rowView.findViewById(R.id.addToPlaylist);
        songNameTxt.setText((songName[position]).replace('_',' '));
        singerNameTxt.setText(singerName[position]);
        songNameTxt.setTypeface(josephSans);
        singerNameTxt.setTypeface(josephSans);
        rowView.setBackgroundColor(Color.rgb(colorArray[position%4][0],colorArray[position%4][1],colorArray[position%4][2]));
        openMusicPlayerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context,musicPlayerService.class);
                intent.putExtra("SONG_NAME", songName[position]);
                intent.putExtra("SINGER_NAME", singerName[position]);
                intent.putExtra("POSITION", position);
                context.startActivity(intent);
            }
        });
        addToPlaylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context,"Added to the Playlist",Toast.LENGTH_SHORT).show();
                editor.putBoolean((position+1)+"",true);
                editor.apply();
            }
        });
        return rowView;

    }
}
