package com.example.soumodippaul.newapp;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class musicPlayerService extends AppCompatActivity {

    private MediaPlayer mPlayer;
    private SeekBar mSeekbar;
    private int startTime;
    private int finishTime;
    private ImageView mPlayPauseButton;
    private ImageView mForwardButton;
    private ImageView mBackwardButton;
    private ImageView addToPlaylistButton;
    private int forwardTime=5000;
    private Handler mHandler = new Handler();
    private final int[][] colorArray=new int[][]{
            {255,204,0},
            {23,166,135},
            {43,135,200},
            {142,76,160}
    };
    RelativeLayout outerLayout;
    SharedPreferences pref;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music_player_service);
        outerLayout=(RelativeLayout) findViewById(R.id.activity_music_player_service);
        pref = getApplicationContext().getSharedPreferences("com.example.soumodippaul.newapp", MODE_PRIVATE);
        editor = pref.edit();
        // GET FROM INTENT
        String songName=getIntent().getStringExtra("SONG_NAME");
        String singerName=getIntent().getStringExtra("SINGER_NAME");
        final int position=getIntent().getIntExtra("POSITION",0);
        outerLayout.setBackgroundColor(Color.rgb(colorArray[position%4][0],colorArray[position%4][1],colorArray[position%4][2]));
        int songID=getResources().getIdentifier((songName.toLowerCase()), "raw","com.example.soumodippaul.newapp");
        mPlayer=MediaPlayer.create(this,songID);
        mSeekbar= (SeekBar) findViewById(R.id.seekBarMusicPlayer);
        startTime=mPlayer.getCurrentPosition();
        finishTime=mPlayer.getDuration();
        mSeekbar.setMax((int)finishTime);
        mPlayer.start();
        mHandler.postDelayed(updateSpinner,100);
        setFontandText(songName,singerName);
        //SET CLICKABLE FALSE
        mSeekbar.setClickable(false);
        mSeekbar.setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        //BUTTONS
        mPlayPauseButton=(ImageView) findViewById(R.id.playPauseMusicPlayer);
        mForwardButton=(ImageView) findViewById(R.id.nextMusicPlayer);
        mBackwardButton=(ImageView) findViewById(R.id.previousMusicPlayer);
        addToPlaylistButton=(ImageView) findViewById(R.id.addToPlaylistMusicPlayer);
        mPlayPauseButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                playPauseMusic();
            }
        });
        mForwardButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                forwardPlayer();
            }
        });
        mBackwardButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                backwardPlayer();
            }
        });
        addToPlaylistButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"Added to the Playlist",Toast.LENGTH_SHORT).show();
                editor.putBoolean((position+1)+"",true);
                editor.apply();
            }
        });
    }

    private void setFontandText(String songNameTxt,String singerNameTxt){
        TextView songName= (TextView) findViewById(R.id.songNameMusicPlayer);
        TextView singerName= (TextView) findViewById(R.id.singerNameMusicPlayer);
        songName.setText(songNameTxt.replace('_',' '));
        singerName.setText(singerNameTxt);
        Typeface josephSans= Typeface.createFromAsset(getApplicationContext().getAssets(),"fonts/josephSans.ttf");
        songName.setTypeface(josephSans);
        singerName.setTypeface(josephSans);
    }

    private void playPauseMusic(){
       if (mPlayer.isPlaying()){
           mPlayPauseButton.setImageResource(R.mipmap.play);
           mPlayer.pause();
       }else{
           mPlayPauseButton.setImageResource(R.mipmap.pause);
           mPlayer.start();
       }
    }

    private void forwardPlayer(){
        int temp = (int)startTime;
        if((temp+forwardTime)<=finishTime){
            startTime = startTime + forwardTime;
            mPlayer.seekTo((int) startTime);
        }else{
            Toast.makeText(getApplicationContext(),"Cannot move forward 5 seconds",Toast.LENGTH_SHORT).show();
        }
    }

    private void backwardPlayer(){
        if (startTime-4000>0){
            startTime=startTime-4000;
            mPlayer.seekTo((int)startTime);
        }else{
            Toast.makeText(getApplicationContext(),"Cannot move backward 4 seconds",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onPanelClosed(int featureId, Menu menu) {
        super.onPanelClosed(featureId, menu);
        mPlayer.stop();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mPlayer.stop();
        finish();
    }

    private Runnable updateSpinner=new Runnable() {
        @Override
        public void run() {
            startTime=mPlayer.getCurrentPosition();
            mSeekbar.setProgress((int)startTime);
            mHandler.postDelayed(this,250);
        }
    };
}
