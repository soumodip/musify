package com.example.soumodippaul.newapp;

import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class playlistActivity extends AppCompatActivity {

    private TextView mainSinger;
    private TextView mainSong;
    private TextView leftSinger;
    private TextView leftSong;
    private TextView rightSinger;
    private TextView rightSong;
    MediaPlayer mPlayer;
    private SeekBar mSeekbar;
    private ImageView mPlayPauseButton;
    private ImageView mForwardButton;
    private ImageView mBackwardButton;
    private ImageView mNextSongButton;
    private ImageView mPreviousSongButton;
    private Handler mHandler = new Handler();
    String[] songNameArray=new String[]{
            "Baatein_Kuch_Ankahee_Si",
            "Bhula_Dena",
            "Bol_Do_Na_Zara",
            "Enna_Sona",
            "Gori_Teri_Ankhen",
            "Love_You_Zindagi",
            "Morey_Saiyan",
            "Piya_Aaye_Na",
            "Pyaar_Manga_Hai",
            "Tujhe_Bhula_Diya"
    };
    String[] singerNameArray=new String[]{
            "Adnan Sami and Pritam",
            "Arijit Singh",
            "Armaan Malik",
            "Arijit Singh",
            "Lucky Ali",
            "Jasleen Royal",
            "Unknown",
            "KK and Jeet Ganguli",
            "Armaan Malik",
            "Mohit Chouhan"
    };
    private  int[] playlistItem=new int[10];
    private int count=0;
    private int position=0;
    public int startTime;
    public int finishTime;
    public int forwardTime=5000;
    public int flag=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playlist);
        //SET FONT
        setFont();
        //LOAD PLAYLIST ARRAY
        loadPlaylistArray();
        //UPDATE MUSIC CONTROLS
        updateControls();
        //UPDATE PLAYLIST
        updatePlaylist(position);
        //ADD ON CLICK LISTENER
        leftSong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position-1>=0){
                    updatePlaylist(position-1);
                }
            }
        });
        rightSong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position+1<count){
                    updatePlaylist(position+1);
                }
            }
        });
    }

    public void setFont(){
        mainSinger=(TextView)findViewById(R.id.mainSingerPlaylist);
        mainSong=(TextView)findViewById(R.id.mainSongPlaylist);
        leftSinger=(TextView)findViewById(R.id.leftSingerPlaylist);
        leftSong=(TextView)findViewById(R.id.leftSongPlaylist);
        rightSinger=(TextView)findViewById(R.id.rightSingerPlaylist);
        rightSong=(TextView)findViewById(R.id.rightSongPlaylist);
        Typeface josephSans= Typeface.createFromAsset(getApplicationContext().getAssets(),"fonts/josephSans.ttf");
        mainSinger.setTypeface(josephSans);
        mainSong.setTypeface(josephSans);
        leftSinger.setTypeface(josephSans);
        leftSong.setTypeface(josephSans);
        rightSinger.setTypeface(josephSans);
        rightSong.setTypeface(josephSans);
    }

    private void loadPlaylistArray(){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("com.example.soumodippaul.newapp", MODE_PRIVATE);
        for (int i=0;i<10;i++){
            if (pref.getBoolean((i+1)+"",false)==true){
                playlistItem[count++]=i;
            }
        }
    }

    private void updatePlaylist(int pos){
        if (count==0){
            finish();
        }else{
            //ONLY ONCE INITIALIZE
            if (flag==0){
                mHandler.postDelayed(updateSpinner,100);
                flag=1;
            }
            position=pos;
            //LEFT
            if (pos>0){
                leftSinger.setText((singerNameArray[playlistItem[pos-1]]));
                leftSong.setText(formatSongName(songNameArray[playlistItem[pos-1]]));
            }else{
                leftSinger.setText("START OF PLAYLIST");
                leftSong.setText("");
            }
            //MIDDLE
            mainSinger.setText((singerNameArray[playlistItem[pos]]));
            mainSong.setText(formatSongName(songNameArray[playlistItem[pos]]));
            //RIGHT
            if (pos+1<count){
                rightSinger.setText(formatSongName(singerNameArray[playlistItem[pos+1]]));
                rightSong.setText(formatSongName(songNameArray[playlistItem[pos+1]]));
            }else{
                rightSinger.setText("END OF PLAYLIST");
                rightSong.setText("");
            }
            playSong(pos);
        }
    }

    private void playSong(int pos){
        if (mPlayer!=null&&mPlayer.isPlaying()){
            mPlayer.release();
        }
        int songID=getResources().getIdentifier((songNameArray[playlistItem[pos]].toLowerCase()), "raw","com.example.soumodippaul.newapp");
        mPlayer=MediaPlayer.create(this,songID);
        startTime=mPlayer.getCurrentPosition();
        finishTime=mPlayer.getDuration();
        mSeekbar.setMax((int)finishTime);
        mPlayer.start();
    }


    private String formatSongName(String songName){
        return songName.replace('_',' ');
    }

    private void updateControls(){
        mSeekbar=(SeekBar) findViewById(R.id.seekBarMusicPlayerPlaylist);
        mPlayPauseButton=(ImageView) findViewById(R.id.playPauseMusicPlayerPlaylist);
        mForwardButton=(ImageView) findViewById(R.id.nextMusicPlayerPlaylist);
        mBackwardButton=(ImageView) findViewById(R.id.previousMusicPlayerPlaylist);
        mNextSongButton=(ImageView) findViewById(R.id.nextSongPlayerPlaylist);
        mPreviousSongButton=(ImageView) findViewById(R.id.previousSongPlayerPlaylist);
        mSeekbar= (SeekBar) findViewById(R.id.seekBarMusicPlayerPlaylist);
        //SET CLICKABLE FALSE
        mSeekbar.setClickable(false);
        mSeekbar.setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        mPlayPauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            playPauseMusic();
            }
        });
        mForwardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forwardPlayer();
            }
        });
        mBackwardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backwardPlayer();
            }
        });
        mNextSongButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position+1<count){
                    updatePlaylist(position+1);
                }else{
                    Toast.makeText(getApplicationContext(),"Playlist has ended",Toast.LENGTH_SHORT).show();
                }
            }
        });
        mPreviousSongButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position-1>=0){
                    updatePlaylist(position-1);
                }else{
                    Toast.makeText(getApplicationContext(),"Cannot move beyond start of Playlist",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void playPauseMusic(){
        if (mPlayer!=null&&mPlayer.isPlaying()) {
            mPlayPauseButton.setImageResource(R.mipmap.play);
            mPlayer.pause();
        }else{
            mPlayPauseButton.setImageResource(R.mipmap.pause);
            mPlayer.start();
        }
    }

    private void forwardPlayer(){
        int temp = (int)startTime;
        if((temp+forwardTime)<=finishTime){
            startTime = startTime + forwardTime;
            mPlayer.seekTo((int) startTime);
        }else{
            Toast.makeText(getApplicationContext(),"Cannot move forward 5 seconds",Toast.LENGTH_SHORT).show();
        }
    }

    private void backwardPlayer(){
        if (startTime-4000>0){
            startTime=startTime-4000;
            mPlayer.seekTo((int)startTime);
        }else{
            Toast.makeText(getApplicationContext(),"Cannot move backward 4 seconds",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mPlayer.stop();
        finish();
    }

    private Runnable updateSpinner=new Runnable() {
        @Override
        public void run() {
            startTime=mPlayer.getCurrentPosition();
            mSeekbar.setProgress((int)startTime);
            mHandler.postDelayed(this,250);
        }
    };

}
