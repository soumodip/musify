<h2><b>Musify</b></h2>
<p><i style="font-size:18px;"><b>Musify</b></i> is a Android Application that can play music. It was designed for Tabs.
</p>
<hr/>
<h3><b>Steps</b></h3>
<ul>
	<li>
        <p>Open it in Android Studio</p>
    </li>
    <li>
        <p>Run it / Build the APK</p>
    </li>
</ul>
<hr/>
<p>Developed by <a href="http://www.soumodippaul.com/">Soumodip Paul</a></p>